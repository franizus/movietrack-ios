//
//  CreateAccountViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 4/30/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmPasswordTextField.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        Auth.auth().createUser(
            withEmail: emailTextField.text!,
            password: passwordTextField.text!) { (result, error) in
                
                if let _ = error {
                    self.showErrorAlert(withMessage: error?.localizedDescription ?? "Error")
                } else {
                    let userInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                    self.present(userInfoViewController, animated: true, completion: nil)
                }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func showErrorAlert(withMessage message:String) {
        let alertController = UIAlertController(title: "Error", message: "Passwords don't match!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
