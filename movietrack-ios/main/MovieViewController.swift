﻿//
//  MovieViewController.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class MovieViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var backDropImage: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var movieId: Int = 0
    var videos: [Video] = []
    var movie: Movie?
    var userMovies: [String] = []
    var isFavourite: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView?.isHidden = true

        // Do any additional setup after loading the view.
        getVideo()
        getMovie()
        getUserMovies()
        
        self.navigationController!.navigationBar.prefersLargeTitles = false
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.tintColor = UIColor.white
        
        self.backDropImage.translatesAutoresizingMaskIntoConstraints = false
        self.backDropImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.backDropImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.backDropImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController!.navigationBar.prefersLargeTitles = true
        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
    }
    
    @objc func favTapped(){
        let userId = String(Auth.auth().currentUser!.uid)
        let db = Firestore.firestore()
        
        if !self.isFavourite {
            if self.userMovies.count == 1 && self.userMovies[0] != "" {
                self.userMovies.append("\(self.movieId)")
            } else {
                self.userMovies = ["\(self.movieId)"]
            }
            db.collection("users").document(userId).updateData([
                "movies": self.userMovies
            ]) { (error) in
                print(error ?? "no error")
            }
        } else {
            let movies = self.userMovies.filter { $0 != "\(self.movieId)" }
            self.userMovies = movies
            db.collection("users").document(userId).updateData([
                "movies": movies
            ]) { (error) in
                print(error ?? "no error")
            }
        }
        
        self.isFavourite = !self.isFavourite
        
        let buttonTitle = self.isFavourite ? "Remove" : "Add"
        let button = UIBarButtonItem(title: buttonTitle, style: .done, target: self, action: #selector(self.favTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func getUserMovies() {
        let userId = String(Auth.auth().currentUser!.uid)
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).getDocument { (document, error) in
            if let document = document, document.exists {
                self.userMovies = document.data()?["movies"] as! [String]
                
                self.isFavourite = self.userMovies.contains("\(self.movieId)")
                let buttonTitle = self.isFavourite ? "Remove" : "Add"
                let button = UIBarButtonItem(title: buttonTitle, style: .done, target: self, action: #selector(self.favTapped))
                self.navigationItem.rightBarButtonItem = button
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func getVideo() {
        let url: String = "https://api.themoviedb.org/3/movie/\(self.movieId)/videos"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<VideosResponse>) in
            self.videos = response.value?.videos ?? []
        }
    }
    
    func getMovie() {
        let url: String = "https://api.themoviedb.org/3/movie/\(self.movieId)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        let processor = BlurImageProcessor(blurRadius: 5.0)
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<Movie>) in
            self.movie = response.value
            self.titleLabel.text = self.movie?.title ?? ""
            self.overviewLabel.text = self.movie?.overview ?? ""
            self.overviewLabel.sizeToFit()
            self.ratingLabel.text = "Rating: \(self.movie?.voteAverage ?? 0.0)"
            self.ratingLabel.sizeToFit()
            self.backDropImage
                .kf
                .setImage(with:
                    URL(string: "https://image.tmdb.org/t/p/w500\(self.movie?.backdropPath ?? "")")!, options: [.processor(processor)])
            self.posterImage
                .kf
                .setImage(with:
                    URL(string: "https://image.tmdb.org/t/p/w500\(self.movie?.posterPath ?? "")")!)
        }
    }
    
    @IBAction func trailerAction(_ sender: Any) {
        let youtubeId = self.videos[0].key
        var url = URL(string:"youtube://\(youtubeId ?? "")")!
        if !UIApplication.shared.canOpenURL(url)  {
            url = URL(string:"http://www.youtube.com/watch?v=\(youtubeId ?? "")")!
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}
