//
//  SettingsViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 6/14/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class SettingsViewController: UIViewController {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moviesLabel: UILabel!
    @IBOutlet weak var seriesLabel: UILabel!
    @IBOutlet weak var moviesButton: UIButton!
    @IBOutlet weak var seriesButton: UIButton!
    
    var series: [String] = []
    var movies: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.seriesButton.isHidden = true
        self.seriesLabel.isHidden = false
        self.moviesButton.isHidden = true
        self.moviesLabel.isHidden = false
        
        // Do any additional setup after loading the view.
        getUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.cornerRadius = userImageView.frame.height / 2.0
        userImageView.layer.masksToBounds = true
    }
    
    func getUserData() {
        let userId = String(Auth.auth().currentUser!.uid)
        let storage = Storage.storage()
        let usersImages = storage.reference().child("users")
        let currentUserImageRef = usersImages.child("\(userId).jpg")
        
        currentUserImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print(error)
            } else {
                let image = UIImage(data: data!)
                self.userImageView.image = image
            }
        }
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).getDocument { (document, error) in
            if let document = document, document.exists {
                self.nameLabel.text = document.data()?["name"] as? String
                self.series = document.data()?["series"] as! [String]
                self.movies = document.data()?["movies"] as! [String]
                
                if self.series.count > 0 && self.series[0] != "" {
                    self.seriesButton.isHidden = false
                    self.seriesLabel.isHidden = true
                }
                
                if self.movies.count > 0 && self.movies[0] != "" {
                    self.moviesButton.isHidden = false
                    self.moviesLabel.isHidden = true
                }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMySeriesSegue" {
            let destination = segue.destination as! MySeriesViewController
            destination.seriesIds = self.series
        }
        if segue.identifier == "toMyMoviesSegue" {
            let destination = segue.destination as! MyMoviesViewController
            destination.moviesIds = self.movies
        }
    }
    
    @IBAction func singOutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            
        }
    }
}
