//
//  SeriesViewController.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import DropDown

class SeriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var seriesTableView: UITableView!
    
    var nextPage = 1
    var series: [Serie] = []
    let rightBarDropDown = DropDown()
    var category: String = "popular"
    var categoryChange: Bool = false
    let categories: [String] = [
        "popular",
        "on_the_air",
        "top_rated",
        "airing_today"
    ]
    var searchController: UISearchController!
    var isFiltering = false
    var filteredSeries = [Serie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.seriesTableView.backgroundColor = UIColor(named: "Color")
        self.seriesTableView.rowHeight = 210
        
        let button = UIBarButtonItem(title: "Category", style: .done, target: self, action: #selector(categoryTapped))
        self.navigationItem.rightBarButtonItem = button
        
        rightBarDropDown.anchorView = button
        prepareDropdown()
        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
        
        // Do any additional setup after loading the view.
        getSeries()
        
        configSearchBar()
    }
    
    func configSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor(named: "Color-1")
        searchController.searchBar.barTintColor = UIColor(named: "Color")
        
        seriesTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredSeries = series.filter {
            ($0.name ?? "")
                .lowercased()
                .contains((searchController.searchBar.text ?? "")
                    .lowercased()
            )
        }
        isFiltering = searchController.searchBar.text != ""
        
        seriesTableView.reloadData()
    }
    
    func prepareDropdown() {
        rightBarDropDown.backgroundColor = UIColor(named: "Color")
        rightBarDropDown.selectionBackgroundColor = UIColor.black
        rightBarDropDown.selectedTextColor = UIColor.white
        rightBarDropDown.textColor = UIColor.white
        rightBarDropDown.dataSource = ["Popular", "On the Air", "Top Rated", "Airing Today"]
        rightBarDropDown.cellConfiguration = { (index, item) in return "\(item)" }
    }
    
    @objc func categoryTapped() {
        rightBarDropDown.selectionAction = { (index: Int, item: String) in
            self.category = self.categories[index]
            self.nextPage = 1
            self.categoryChange = true
            self.getSeries()
        }
        
        rightBarDropDown.width = 140
        rightBarDropDown.bottomOffset = CGPoint(x: 0, y:(rightBarDropDown.anchorView?.plainView.bounds.height)!)
        rightBarDropDown.show()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? self.filteredSeries.count : self.series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "seriesCell") as! SeriesTableViewCell
            var currentSerie: Serie!
            
            if isFiltering {
                currentSerie = filteredSeries[indexPath.row]
            } else {
                currentSerie = series[indexPath.row]
            }
            
            let year = currentSerie.releaseDate?.split(separator: "-")[0]
            
            cell.titleLabel.text = currentSerie.name?.capitalized ?? "n/a"
            cell.yearLabel.text = String(year ?? "")
            cell.backDropImageView
                .kf
                .setImage(with:
                    URL(string: "https://image.tmdb.org/t/p/w500\(currentSerie.backdropPath ?? "")")!)
            
            cell.contentView.backgroundColor = UIColor(named: "Color")
            let cellBGView = UIView()
            cellBGView.backgroundColor = UIColor.black
            cell.selectedBackgroundView = cellBGView
            
            return cell
    }
    
    func getSeries() {
        let url: String = "https://api.themoviedb.org/3/tv/\(self.category)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
            "page": nextPage
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<SeriesResponse>) in
            if self.categoryChange {
                self.series = response.value?.series ?? []
            } else {
                self.series += response.value?.series ?? []
            }
            self.seriesTableView.reloadData()
            self.nextPage += 1
            self.categoryChange = false
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == series.count - 1 {
            getSeries()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSerieSegue" {
            let destination = segue.destination as! SerieViewController
            let selectedSerie = series[seriesTableView.indexPathForSelectedRow?.row ?? 0]
            destination.serieId = selectedSerie.id!
        }
    }

}
