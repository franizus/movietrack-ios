//
//  MainViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 5/10/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import DropDown

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var moviesTableView: UITableView!
    
    var nextPage = 1
    var movies: [Movie] = []
    let rightBarDropDown = DropDown()
    var category: String = "popular"
    var categoryChange: Bool = false
    let categories: [String] = [
        "popular",
        "now_playing",
        "top_rated",
        "upcoming"
    ]
    var searchController: UISearchController!
    var isFiltering = false
    var filteredMovies = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moviesTableView.backgroundColor = UIColor(named: "Color")
        self.moviesTableView.rowHeight = 160
        
        let button = UIBarButtonItem(title: "Category", style: .done, target: self, action: #selector(categoryTapped))
        self.navigationItem.rightBarButtonItem = button
        
        rightBarDropDown.anchorView = button
        prepareDropdown()
        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
        
        // Do any additional setup after loading the view.
        getMovies()
        
        configSearchBar()
    }
    
    func configSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor(named: "Color-1")
        searchController.searchBar.barTintColor = UIColor(named: "Color")
        
        moviesTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredMovies = movies.filter {
            ($0.title ?? "")
                .lowercased()
                .contains((searchController.searchBar.text ?? "")
                    .lowercased()
            )
        }
        isFiltering = searchController.searchBar.text != ""
        
        moviesTableView.reloadData()
    }
    
    func prepareDropdown() {
        rightBarDropDown.backgroundColor = UIColor(named: "Color")
        rightBarDropDown.selectionBackgroundColor = UIColor.black
        rightBarDropDown.selectedTextColor = UIColor.white
        rightBarDropDown.textColor = UIColor.white
        rightBarDropDown.dataSource = ["Popular", "Now Playing", "Top Rated", "Upcoming"]
        rightBarDropDown.cellConfiguration = { (index, item) in return "\(item)" }
    }
    
    @objc func categoryTapped() {
        rightBarDropDown.selectionAction = { (index: Int, item: String) in
            self.category = self.categories[index]
            self.nextPage = 1
            self.categoryChange = true
            self.getMovies()
        }
        
        rightBarDropDown.width = 140
        rightBarDropDown.bottomOffset = CGPoint(x: 0, y:(rightBarDropDown.anchorView?.plainView.bounds.height)!)
        rightBarDropDown.show() 
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? self.filteredMovies.count : self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            tableView
                .dequeueReusableCell(withIdentifier: "moviesCell") as! MoviesTableViewCell
        var currentMovie: Movie!
        
        if isFiltering {
            currentMovie = filteredMovies[indexPath.row]
        } else {
            currentMovie = movies[indexPath.row]
        }
        
        let local = NSLocale(localeIdentifier: "en")
        let year = currentMovie.releaseDate?.split(separator: "-")[0]
        
        cell.titleLabel.text = currentMovie.title?.capitalized ?? "n/a"
        cell.languageLabel.text = local.displayName(forKey: NSLocale.Key.identifier, value: currentMovie.releaseDate ?? "en") ?? "English"
        cell.yearLabel.text = String(year ?? "") 
        
        cell.posterImageView
            .kf
            .setImage(with:
                URL(string: "https://image.tmdb.org/t/p/w500\(currentMovie.posterPath ?? "")")!)
        cell.contentView.backgroundColor = UIColor(named: "Color")
        let cellBGView = UIView()
        cellBGView.backgroundColor = UIColor.black
        cell.selectedBackgroundView = cellBGView
        return cell
    }
    
    func getMovies() {
        let url: String = "https://api.themoviedb.org/3/movie/\(self.category)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
            "page": nextPage,
            "region": "us"
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<MoviesResponse>) in
            if self.categoryChange {
                self.movies = response.value?.movies ?? []
            } else {
                self.movies += response.value?.movies ?? []
            }
            self.moviesTableView.reloadData()
            self.nextPage += 1
            self.categoryChange = false
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == movies.count - 1 {
            getMovies()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMovieSegue" {
            let destination = segue.destination as! MovieViewController
            let selectedMovie = movies[moviesTableView.indexPathForSelectedRow?.row ?? 0]
            destination.movieId = selectedMovie.id!
        }
    }
    
}
