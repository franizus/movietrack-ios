//
//  SerieViewController.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class SerieViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backDropImageView: UIImageView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var serieId: Int = 0
    var videos: [Video] = []
    var serie: Serie?
    var userSeries: [String] = []
    var isFavourite: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView?.isHidden = true
        
        // Do any additional setup after loading the view.
        getVideo()
        getSerie()
        getUserSeries()
        
        self.navigationController!.navigationBar.prefersLargeTitles = false
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.tintColor = UIColor.white
        
        self.backDropImageView.translatesAutoresizingMaskIntoConstraints = false
        self.backDropImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.backDropImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.backDropImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController!.navigationBar.prefersLargeTitles = true
        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
    }
    
    @objc func favTapped(){
        let userId = String(Auth.auth().currentUser!.uid)
        let db = Firestore.firestore()
        
        if !self.isFavourite {
            if self.userSeries.count == 1 && self.userSeries[0] != "" {
                self.userSeries.append("\(self.serieId)")
            } else {
                self.userSeries = ["\(self.serieId)"]
            }
            db.collection("users").document(userId).updateData([
                "series": self.userSeries
            ]) { (error) in
                print(error ?? "no error")
            }
        } else {
            let series = self.userSeries.filter { $0 != "\(self.serieId)" }
            self.userSeries = series
            db.collection("users").document(userId).updateData([
                "series": series
            ]) { (error) in
                print(error ?? "no error")
            }
        }
        
        self.isFavourite = !self.isFavourite
        
        let buttonTitle = self.isFavourite ? "Remove" : "Add"
        let button = UIBarButtonItem(title: buttonTitle, style: .done, target: self, action: #selector(self.favTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func getUserSeries() {
        let userId = String(Auth.auth().currentUser!.uid)
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).getDocument { (document, error) in
            if let document = document, document.exists {
                self.userSeries = document.data()?["series"] as! [String]
                
                self.isFavourite = self.userSeries.contains("\(self.serieId)")
                let buttonTitle = self.isFavourite ? "Remove" : "Add"
                let button = UIBarButtonItem(title: buttonTitle, style: .done, target: self, action: #selector(self.favTapped))
                self.navigationItem.rightBarButtonItem = button
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func getVideo() {
        let url: String = "https://api.themoviedb.org/3/tv/\(self.serieId)/videos"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<VideosResponse>) in
            self.videos = response.value?.videos ?? []
        }
    }
    
    func getSerie() {
        let url: String = "https://api.themoviedb.org/3/tv/\(self.serieId)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        let processor = BlurImageProcessor(blurRadius: 5.0)
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<Serie>) in
            self.serie = response.value
            self.titleLabel.text = self.serie?.name ?? ""
            self.overviewLabel.text = self.serie?.overview ?? ""
            self.overviewLabel.sizeToFit()
            self.ratingLabel.text = "Rating: \(self.serie?.voteAverage ?? 0.0)"
            self.ratingLabel.sizeToFit()
            self.backDropImageView
                .kf
                .setImage(with:
                    URL(string: "https://image.tmdb.org/t/p/w500\(self.serie?.backdropPath ?? "")")!, options: [.processor(processor)])
            self.posterImageView
                .kf
                .setImage(with:
                    URL(string: "https://image.tmdb.org/t/p/w500\(self.serie?.posterPath ?? "")")!)
        }
    }

    @IBAction func playTrailer(_ sender: Any) {
        let youtubeId = self.videos[0].key
        var url = URL(string:"youtube://\(youtubeId ?? "")")!
        if !UIApplication.shared.canOpenURL(url)  {
            url = URL(string:"http://www.youtube.com/watch?v=\(youtubeId ?? "")")!
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
