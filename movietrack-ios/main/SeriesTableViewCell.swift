//
//  SeriesTableViewCell.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit

class SeriesTableViewCell: UITableViewCell {
    @IBOutlet weak var backDropImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
