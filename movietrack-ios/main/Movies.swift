//
//  Movies.swift
//  movietrack-ios
//
//  Created by Bryan Mosquera on 7/21/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import Foundation
import ObjectMapper

class MoviesResponse: Mappable {
    var movies: [Movie]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        movies <- map["results"]
    }
}

class Movie: Mappable {
    var id: Int?
    var title: String?
    var posterPath: String?
    var backdropPath: String?
    var releaseDate: String?
    var overview: String?
    var originalLanguage: String?
    var voteAverage: Float?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        posterPath <- map["poster_path"]
        backdropPath <- map["backdrop_path"]
        releaseDate <- map["release_date"]
        overview <- map["overview"]
        originalLanguage <- map["original_language"]
        voteAverage <- map["vote_average"]
    }
}

class VideosResponse: Mappable {
    var videos: [Video]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        videos <- map["results"]
    }
}

class Video: Mappable {
    var id: String?
    var key: String?
    var name: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        key <- map["key"]
        name <- map["name"]
    }
}
