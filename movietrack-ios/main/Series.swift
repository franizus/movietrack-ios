//
//  Movies.swift
//  movietrack-ios
//
//  Created by Bryan Mosquera on 7/21/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import Foundation
import ObjectMapper

class SeriesResponse: Mappable {
    var series: [Serie]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        series <- map["results"]
    }
}

class Serie: Mappable {
    var id: Int?
    var name: String?
    var posterPath: String?
    var backdropPath: String?
    var releaseDate: String?
    var overview: String?
    var voteAverage: Float?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        posterPath <- map["poster_path"]
        backdropPath <- map["backdrop_path"]
        releaseDate <- map["first_air_date"]
        overview <- map["overview"]
        voteAverage <- map["vote_average"]
    }
}
