//
//  MySeriesViewController.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class MySeriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var seriesTableView: UITableView!
    
    var seriesIds: [String] = []
    var series: [Serie] = []
    var searchController: UISearchController!
    var isFiltering = false
    var filteredSeries = [Serie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.seriesTableView.backgroundColor = UIColor(named: "Color")
        self.seriesTableView.rowHeight = 210
        // Do any additional setup after loading the view.
        getSeries()
        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
        
        configSearchBar()
    }
    
    func configSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor(named: "Color-1")
        searchController.searchBar.barTintColor = UIColor(named: "Color")
        
        seriesTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredSeries = series.filter {
            ($0.name ?? "")
                .lowercased()
                .contains((searchController.searchBar.text ?? "")
                    .lowercased()
            )
        }
        isFiltering = searchController.searchBar.text != ""
        
        seriesTableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? self.filteredSeries.count : self.series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "seriesCell") as! SeriesTableViewCell
        var currentSerie: Serie!
        
        if isFiltering {
            currentSerie = filteredSeries[indexPath.row]
        } else {
            currentSerie = series[indexPath.row]
        }
        
        let year = currentSerie.releaseDate?.split(separator: "-")[0]
        
        cell.titleLabel.text = currentSerie.name?.capitalized ?? "n/a"
        cell.yearLabel.text = String(year ?? "")
        cell.backDropImageView
            .kf
            .setImage(with:
                URL(string: "https://image.tmdb.org/t/p/w500\(currentSerie.backdropPath ?? "")")!)
        
        cell.contentView.backgroundColor = UIColor(named: "Color")
        let cellBGView = UIView()
        cellBGView.backgroundColor = UIColor.black
        cell.selectedBackgroundView = cellBGView
        
        return cell
    }
    
    func getSeries() {
        for id in seriesIds {
            getSerie(id: id)
        }
    }
    
    func getSerie(id: String) {
        let url: String = "https://api.themoviedb.org/3/tv/\(id)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<Serie>) in
            let serie = response.value
            self.series.append(serie!)
            self.seriesTableView.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSerieSegue1" {
            let destination = segue.destination as! SerieViewController
            let selectedSerie = series[seriesTableView.indexPathForSelectedRow?.row ?? 0]
            destination.serieId = selectedSerie.id!
        }
    }
    
}
