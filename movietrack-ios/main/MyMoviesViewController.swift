//
//  MyMoviesViewController.swift
//  movietrack-ios
//
//  Created by Francisco on 8/4/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class MyMoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var moviesTableView: UITableView!
    
    var moviesIds: [String] = []
    var movies: [Movie] = []
    var searchController: UISearchController!
    var isFiltering = false
    var filteredMovies = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moviesTableView.backgroundColor = UIColor(named: "Color")
        self.moviesTableView.rowHeight = 160

        self.navigationController!.navigationBar.tintColor = UIColor(named: "Color-1")
        
        // Do any additional setup after loading the view.
        getMovies()
        
        configSearchBar()
    }
    
    func configSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor(named: "Color-1")
        searchController.searchBar.barTintColor = UIColor(named: "Color")
        
        moviesTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredMovies = movies.filter {
            ($0.title ?? "")
                .lowercased()
                .contains((searchController.searchBar.text ?? "")
                    .lowercased()
            )
        }
        isFiltering = searchController.searchBar.text != ""
        
        moviesTableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? self.filteredMovies.count : self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            tableView
                .dequeueReusableCell(withIdentifier: "moviesCell") as! MoviesTableViewCell
        var currentMovie: Movie!
        
        if isFiltering {
            currentMovie = filteredMovies[indexPath.row]
        } else {
            currentMovie = movies[indexPath.row]
        }
        
        let local = NSLocale(localeIdentifier: "en")
        let year = currentMovie.releaseDate?.split(separator: "-")[0]
        
        cell.titleLabel.text = currentMovie.title?.capitalized ?? "n/a"
        cell.languageLabel.text = local.displayName(forKey: NSLocale.Key.identifier, value: currentMovie.releaseDate ?? "en") ?? "English"
        cell.yearLabel.text = String(year ?? "")
        
        cell.posterImageView
            .kf
            .setImage(with:
                URL(string: "https://image.tmdb.org/t/p/w500\(currentMovie.posterPath ?? "")")!)
        cell.contentView.backgroundColor = UIColor(named: "Color")
        let cellBGView = UIView()
        cellBGView.backgroundColor = UIColor.black
        cell.selectedBackgroundView = cellBGView
        return cell
    }
    
    func getMovies() {
        for id in moviesIds {
            getMovie(id: id)
        }
    }
    
    func getMovie(id: String) {
        let url: String = "https://api.themoviedb.org/3/movie/\(id)"
        print(url)
        let params: Parameters = [
            "api_key": "d86068144f769b45826958d1251e8f6d",
            "language": "en-US",
        ]
        
        AF.request(url, method: .get, parameters: params).responseObject { (response: DataResponse<Movie>) in
            let movie = response.value
            self.movies.append(movie!)
            self.moviesTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMovieSegue1" {
            let destination = segue.destination as! MovieViewController
            let selectedMovie = movies[moviesTableView.indexPathForSelectedRow?.row ?? 0]
            destination.movieId = selectedMovie.id!
        }
    }

}
