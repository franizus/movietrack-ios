//
//  UserInfoViewController.swift
//  ios-app
//
//  Created by Francisco Izurieta on 5/10/19.
//  Copyright © 2019 Francisco Izurieta. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyyy"
        return formatter
    }()
    
    var formatted: String {
        return Date.formatter.string(from: self)
    }
}

class UserInfoViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nickTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    
    let datePickerView = UIDatePicker()
    let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        birthdayTextField.text = datePickerView.date.formatted
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.cornerRadius = userImageView.frame.height / 2.0
        userImageView.layer.masksToBounds = true
        birthdayTextField.inputView = datePickerView
    }
    
    @IBAction func addPictureButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("error picking image")
            return
        }
        
        userImageView.image = image
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let userId = String(Auth.auth().currentUser!.uid)
        let storage = Storage.storage()
        let usersImages = storage.reference().child("users")
        let currentUserImageRef = usersImages.child("\(userId).jpg")
        let userImage = userImageView.image
        let data = userImage?.jpegData(compressionQuality: 1)
        let uploadTask = currentUserImageRef.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            let size = metadata.size
            currentUserImageRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
            }
        }
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).setData([
            "name" : nameTextField.text ?? "",
            "nick" : nickTextField.text ?? "",
            "birthdate": datePickerView.date,
            "registerCompleted": true,
            "movies": [""],
            "series": [""]
        ]) { (error) in
            print(error ?? "no error")
        }
        
        self.performSegue(withIdentifier: "toTabUser", sender: self)
    }
    
}
